#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_TEAM 4
#define MAX_NAME 25
typedef struct team_s team_t;
struct team_s {
	char *name;
	int match;
	int point;
	int goal;
	int lose;
	int rank;
};

team_t team[MAX_TEAM];
void get_input() {
	int match_cnt=1;
	for (match_cnt = 1; match_cnt <= 5; match_cnt++) {
		char team_name_1[MAX_NAME];
		char team_name_2[MAX_NAME];
		char result[4];
		scanf("%s", team_name_1);
		scanf("%s", team_name_2);
		scanf("%s", result);
		int team_goal_1 = result[0] - '0'; //since goal is limited in range 0-9
		int team_goal_2 = result[2] - '0';

		int team_win = 0; //0 - draw, 1 - team_1 win, 2 - team_2 win
		if (team_goal_1 > team_goal_2) {
			team_win = 1;
		} else if (team_goal_1 < team_goal_2) {
			team_win = 2;
		} else {
			team_win = 0;
		}
		int team_exist_1 = 0;
		int team_exist_2 = 0;
		int team_idx=0;
		for (team_idx=0; team_idx<MAX_TEAM; team_idx++) {
			if (team[team_idx].name) {
				if (strcmp(team[team_idx].name, team_name_1) == 0) {
					team_exist_1 = 1;
				}
				if (strcmp(team[team_idx].name, team_name_2) == 0) {
					team_exist_2 = 1;
				}
			}
		}
		if (!team_exist_1 || !team_exist_2) {
			for (team_idx=0; team_idx < MAX_TEAM; team_idx++) {
				if (team_exist_1 == 0) {
					if (team[team_idx].name == NULL) {
						team[team_idx].name = malloc(sizeof(char)*MAX_NAME);
						strcpy(team[team_idx].name, team_name_1);
						team_exist_1 = 1;
					}
				}
				if (team_exist_2 == 0) {
					if (team[team_idx].name == NULL) {
						team[team_idx].name = malloc(sizeof(char)*MAX_NAME);
						strcpy(team[team_idx].name, team_name_2);
						team_exist_2 = 1;
					}
				}
			}
		} 
		for (team_idx=0; team_idx<MAX_TEAM; team_idx++) {
			if (team[team_idx].name) {
				if (strcmp(team[team_idx].name, team_name_1) == 0) {
					team[team_idx].match++;
					team[team_idx].goal += team_goal_1;
					team[team_idx].lose += team_goal_2;
					switch (team_win) {
						case 0:
							team[team_idx].point += 1;
							break;
						case 1:
							team[team_idx].point += 3;
							break;
						case 2:
							team[team_idx].point += 0;
							break;
					}
				}
				if (strcmp(team[team_idx].name, team_name_2) == 0) {
					team[team_idx].match++;
					team[team_idx].goal += team_goal_2;
					team[team_idx].lose += team_goal_1;
					switch (team_win) {
						case 0:
							team[team_idx].point += 1;
							break;
						case 1:
							team[team_idx].point += 0;
							break;
						case 2:
							team[team_idx].point += 3;
							break;
					}
				}
			}
		}
	}
}
int proccess() {
	int M=0, N=0;
	team_t *vietnam = NULL;
	team_t *oposite = NULL;
	int team_idx=0;
	for (team_idx=0; team_idx<MAX_TEAM; team_idx++) {
		if (strcmp(team[team_idx].name, "VIETNAM") == 0) {
			vietnam = &team[team_idx];
		} else if (team[team_idx].match == 2) {
			oposite = &team[team_idx];
		}
	}
	int m=0;
	int n=0;
	for (m=0; m<=9; m++) {
		M=m; //vietname score
		for (n=0; n<=9; n++) {
			N=n; //oposite score
			vietnam->goal += m;
			vietnam->lose += n;
			oposite->goal += n;
			oposite->lose += m;
			int v_plus = 0;
			int o_plus = 0;
			if (m > n) {
				v_plus = 3;
				o_plus = 0;
			} else if (m < n) {
				v_plus = 0;
				o_plus = 3;
			} else {
				v_plus = 1;
				o_plus = 1;
			}
			vietnam->point += v_plus;
			oposite->point += o_plus;
			//ranking here, by point => diff => total goal => alphabeta
			int i=0;
			int j=0;
			while (1) {
				//sort by point
				int rank[MAX_TEAM];
				for (i=0; i<MAX_TEAM; i++) {
					int tmp_rank = team[i].rank;
					int tmp_point = team[i].point;
					rank[i] = team[i].rank;
					for (j=0; j<MAX_TEAM; j++) {
						if (team[j].point > tmp_point && i != j && tmp_rank == team[j].rank) {
							rank[i]++;
						}
					}
				}
				for (i=0; i<MAX_TEAM; i++) {
					team[i].rank = rank[i];
				}
				for (i=0; i<MAX_TEAM; i++) {
					int tmp_rank = team[i].rank;
					int tmp_diff = team[i].goal - team[i].lose;
					rank[i] = team[i].rank;
					for (j=0; j<MAX_TEAM; j++) {
						int o_diff = team[j].goal - team[j].lose;
						if (o_diff > tmp_diff && i != j && tmp_rank == team[j].rank) {
							rank[i]++;
						}
					}
				}
				for (i=0; i<MAX_TEAM; i++) {
					team[i].rank = rank[i];
				}
				for (i=0; i<MAX_TEAM; i++) {
					int tmp_rank = team[i].rank;
					int tmp_goal = team[i].goal;
					rank[i] = team[i].rank;
					for (j=0; j<MAX_TEAM; j++) {
						if (team[j].goal > tmp_goal && i != j && tmp_rank == team[j].rank) {
							rank[i]++;
						}
					}
				}
				for (i=0; i<MAX_TEAM; i++) {
					team[i].rank = rank[i];
				}
				for (i=0; i<MAX_TEAM; i++) {
					int tmp_rank = team[i].rank;
					rank[i] = team[i].rank;
					for (j=0; j<MAX_TEAM; j++) {
						if (strcmp(team[j].name, team[i].name) < 0 && i != j && tmp_rank == team[j].rank) {
							rank[i]++;
						}
					}
				}
				for (i=0; i<MAX_TEAM; i++) {
					team[i].rank = rank[i];
				}
				int flag = 0;
				for (i=0; i<MAX_TEAM; i++) {
					int tmp_rank = team[i].rank;
					for (j=0; j < MAX_TEAM; j++) {
						if (tmp_rank == team[j].rank && i != j) {
							flag = 1;
						}
					}
				}
				if (flag == 0) {
					break;
				}
			}
			if (vietnam->rank == 0 || vietnam->rank == 1) {
				printf("%d:%d", M, N);
				return 1;
			}
			vietnam->goal -= m;
			vietnam->lose -= n;
			vietnam->point -= v_plus;
			oposite->goal -= n;
			oposite->lose -= m;
			oposite->point -= o_plus;
			for (i=0; i<MAX_TEAM; i++) {
				team[i].rank = 0;
			}
		}
	}
	return 0;
}
int main () {
	memset(&team, 0, sizeof(team_t)*MAX_TEAM);
	get_input();
	int result = proccess();
	if (result == 0 ) {
		printf("IMPOSSIBLE");
	}
	return 0;
}
